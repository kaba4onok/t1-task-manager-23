package ru.t1.rleonov.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    private static final String ARGUMENT = "-v";

    @NotNull
    private static final String NAME = "version";

    @NotNull
    private static final String DESCRIPTION = "Show application version.";

    @NotNull
    private static final String VERSION = "1.23.0";

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(VERSION);
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}

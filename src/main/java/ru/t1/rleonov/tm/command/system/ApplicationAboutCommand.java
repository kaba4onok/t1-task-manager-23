package ru.t1.rleonov.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private static final  String ARGUMENT = "-a";

    @NotNull
    private static final  String NAME = "about";

    @NotNull
    private static final  String DESCRIPTION = "Show application info.";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Task Manager");
        System.out.println("Developer: Roman Leonov");
        System.out.println("Gitlab: https://gitlab.com/kaba4onok/");
        System.out.println("E-mail: rleonov@t1-consulting.ru");
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
